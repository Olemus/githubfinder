import React, { Fragment, useEffect, useContext } from "react";
import Spinner from "../layout/Spinner";
import { Link } from "react-router-dom";
import Repos from "../repos/Repos";
import GithubContext from '../../context/github/githubContext';

const User = ({ match}) => {

  const githubContext = useContext(GithubContext);

  const {getUser, getUserRepos, user, repos, loading} = githubContext;

  useEffect(() => {
    getUser(match.params.login);
    getUserRepos(match.params.login);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])
 
    const {
      hireable,
      login,
      avatar_url,
      location,
      bio,
      name,
      html_url,
      company,
      blog,
      followers,
      following, 
      public_repos,
      public_gists      
    } = user;

    if (loading) return <Spinner />

    return (
      <Fragment>
                <Link to="/" className="btn btn-light">
          Back To Search
        </Link>
        <p></p>

        <div className="card grid-2">
          <div className="all-center">
            <img
              src={avatar_url}
              alt="user avatar"
              className="round-img"
              style={{ width: "150px" }}
            />
            <h1>{name}</h1>
            <p>Location: {location}</p>
          </div>
          <div>         
            <ul>
              <li>
                {login && (
                  <Fragment>
                    <strong>Username: </strong> {login}
                  </Fragment>
                )}
              </li>
              <li>
                <Fragment>
                  <strong>Hireable: </strong>
                  {hireable ? (
                    <i className="fa fa-check text-success"></i>
                  ) : (
                    <i className="fa fa-times-circle text-danger"></i>
                  )}
                </Fragment>
              </li>
              <li>
                {company && (
                  <Fragment>
                    <strong>Company: </strong> {company}
                  </Fragment>
                )}
              </li>
              <li>
                {blog && (
                  <Fragment>
                    <strong>Website: </strong> {blog}
                  </Fragment>
                )}
              </li>
            </ul>
            {bio && (
              <Fragment>
                <p className='my-1'>{bio}</p>
              </Fragment>
            )}
            <a href={html_url} className="btn btn-dark my-1">
              Visit Github Profile
            </a>
          </div>
        </div>      
        <div className = "card text-center">
          <div className='badge badge-primary'>Followers: {followers}</div>
          <div className='badge badge-success'>Following: {following}</div>
          <div className='badge badge-light'>Public Repos: {public_repos}</div>
          <div className='badge badge-dark'>Public Gists: {public_gists}</div>
        </div> 

               
        {loading ? <Spinner /> : <Repos repos = {repos} />}
      </Fragment>
    );
}


export default User;
