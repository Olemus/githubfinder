import React, {useContext} from "react";
import UserInfo from "./Useritem";
import Spinner from "../layout/Spinner";
import GithubContext from '../../context/github/githubContext';


const Users = () => {

  const githubContext = useContext(GithubContext);

  return githubContext.loading ? (
    <Spinner />
  ) : (
    <div style={userStyle}>
      {githubContext.users.map(user => (
        <UserInfo key={user.id} user={user} />
      ))}
    </div>
  );
};

const userStyle = {
  display: "grid",
  gridTemplateColumns: "repeat(3, 1fr)",
  gridGap: "1rem"
};

export default Users;
